import command from '$lib/command';
import { writable, derived } from 'svelte/store';
import { asyncable } from 'svelte-asyncable';

// Creates an asyncable store with a "reload trigger".
// This reload trigger makes sure the getter gets called and triggers subscriptions
const asyncableReload = (getter, setter, stores = []) => {
  const reloadTrigger = writable(false);
  const store = asyncable(getter, setter, [...stores, reloadTrigger]);

  const reload = () => reloadTrigger.update((value) => !value);

  return {
    ...store,
    reload
  };
};

export const selectedGame = writable(null);
export const selectedLocalGame = writable(null);
export const selectedGenre = writable(null);
export const page = writable(1);
export const query = writable('');

export const databaseGames = asyncable(
  async ($selectedGenre, $page, $query) => {
    let opts = {
      page_number: $page - 1,
      page_size: 20,
      filter_genres: [],
      filter_languages: [],
      filter_tags: []
    };

    if ($query != '') {
      opts.search = $query;
    }

    if ($selectedGenre !== '' && $selectedGenre !== null) {
      opts.filter_genres.push($selectedGenre);
    }

    selectedGame.set(null);

    return await command.database('get_games', { opts });
  },
  null,
  [selectedGenre, page, query]
);

export const genres = asyncable(async () => await command.database('get_genres'), null);

export const mode = writable('grid');
export const sidebarActive = writable(false);
export const isAdmin = writable(false);

export const actions = writable({});

export const load = async () => {
  isAdmin.set(await command.database('is_admin'));
};
