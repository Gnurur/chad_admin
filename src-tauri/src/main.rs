#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

mod command;

use chad_rs::{banner::BannerFetcher, database::DatabaseFetcher, scraper::LeetxScraper};

fn main() {
    // Should improve performance
    std::env::set_var("WEBKIT_FORCE_COMPOSITING_MODE", "1");

    let supabase_key = std::env::var("SUPABASE_SECRET_KEY")
        .unwrap_or(chad_rs::database::SUPABASE_PUBLIC_API_KEY.into());

    let steamgriddb_key = std::env::var("STEAMGRIDDB_KEY").unwrap_or(String::new());

    let database = DatabaseFetcher::new(chad_rs::database::SUPABASE_ENDPOINT, &supabase_key);
    let scraper = LeetxScraper::default();
    let banner = BannerFetcher::new(&steamgriddb_key);

    tauri::Builder::default()
        .manage(database)
        .manage(scraper)
        .manage(banner)
        .invoke_handler(tauri::generate_handler![
            // Database
            command::database::database_get_games,
            command::database::database_get_genres,
            command::database::database_get_languages,
            command::database::database_get_tags,
            command::database::database_add_update_game,
            command::database::database_remove_game,
            command::database::database_add_genres,
            command::database::database_add_languages,
            command::database::database_add_tags,
            command::database::database_delete_genres,
            command::database::database_delete_languages,
            command::database::database_delete_tags,
            command::database::database_is_admin,
            command::database::database_upload_banner_url,
            command::database::database_upload_banner_file,
            // Scraper
            command::scraper::scraper_get_all_new_games,
            command::scraper::scraper_get_new_games_pages,
            command::scraper::scraper_apply_actions,
            command::scraper::scraper_find_banners_for_game,
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
