use super::TauriChadError;
use chad_rs::database::{
    self,
    table::{self, GameKey},
    DatabaseFetcher, GetGamesOpts,
};
use serde::Serialize;
use std::path::PathBuf;

#[tauri::command]
pub async fn database_get_games(
    opts: GetGamesOpts,
    fetcher: tauri::State<'_, DatabaseFetcher>,
) -> Result<Vec<database::Game>, TauriChadError> {
    fetcher.get_games(&opts).await.map_err(|e| e.into())
}

#[tauri::command]
pub async fn database_get_genres(
    fetcher: tauri::State<'_, DatabaseFetcher>,
) -> Result<Vec<String>, TauriChadError> {
    fetcher
        .list_items::<table::ListGenres>()
        .await
        .map_err(|e| TauriChadError::from(e))
}

#[tauri::command]
pub async fn database_get_languages(
    fetcher: tauri::State<'_, DatabaseFetcher>,
) -> Result<Vec<String>, TauriChadError> {
    fetcher
        .list_items::<table::ListLanguages>()
        .await
        .map_err(|e| TauriChadError::from(e))
}

#[tauri::command]
pub async fn database_get_tags(
    fetcher: tauri::State<'_, DatabaseFetcher>,
) -> Result<Vec<String>, TauriChadError> {
    fetcher
        .list_items::<table::ListTags>()
        .await
        .map_err(|e| TauriChadError::from(e))
}

#[tauri::command]
pub async fn database_is_admin(
    fetcher: tauri::State<'_, DatabaseFetcher>,
) -> Result<bool, TauriChadError> {
    fetcher
        .is_admin()
        .await
        .map_err(|e| TauriChadError::from(e))
}

#[tauri::command]
pub async fn database_add_update_game(
    fetcher: tauri::State<'_, DatabaseFetcher>,
    game: database::Game,
) -> Result<(), TauriChadError> {
    fetcher
        .add_update_game(
            &game.game,
            &game.languages.into_iter().collect::<Vec<_>>(),
            &game.genres.into_iter().collect::<Vec<_>>(),
            &game.tags.into_iter().collect::<Vec<_>>(),
        )
        .await
        .map_err(|e| TauriChadError::from(e))
}

#[tauri::command]
pub async fn database_remove_game(
    fetcher: tauri::State<'_, DatabaseFetcher>,
    game_key: GameKey<'_>,
) -> Result<(), TauriChadError> {
    fetcher
        .remove_game(&game_key)
        .await
        .map_err(|e| TauriChadError::from(e))
}

async fn add_items<T: table::Item + Serialize>(
    fetcher: &DatabaseFetcher,
    game_key: GameKey<'_>,
    items: &[String],
) -> Result<(), TauriChadError> {
    fetcher
        .add_items::<T>(&game_key, items)
        .await
        .map_err(|e| TauriChadError::from(e))
}

#[tauri::command]
pub async fn database_add_genres(
    fetcher: tauri::State<'_, DatabaseFetcher>,
    game_key: GameKey<'_>,
    genres: Vec<String>,
) -> Result<(), TauriChadError> {
    add_items::<table::Genre>(&fetcher, game_key, &genres).await
}

#[tauri::command]
pub async fn database_add_languages(
    fetcher: tauri::State<'_, DatabaseFetcher>,
    game_key: GameKey<'_>,
    languages: Vec<String>,
) -> Result<(), TauriChadError> {
    add_items::<table::Language>(&fetcher, game_key, &languages).await
}

#[tauri::command]
pub async fn database_add_tags(
    fetcher: tauri::State<'_, DatabaseFetcher>,
    game_key: GameKey<'_>,
    tags: Vec<String>,
) -> Result<(), TauriChadError> {
    add_items::<table::Genre>(&fetcher, game_key, &tags).await
}

async fn delete_items<T: table::Item + Serialize>(
    fetcher: &DatabaseFetcher,
    game_key: GameKey<'_>,
    items: &[String],
) -> Result<(), TauriChadError> {
    fetcher
        .delete_items::<T>(&game_key, items)
        .await
        .map_err(|e| TauriChadError::from(e))
}

#[tauri::command]
pub async fn database_delete_genres(
    fetcher: tauri::State<'_, DatabaseFetcher>,
    game_key: GameKey<'_>,
    genres: Vec<String>,
) -> Result<(), TauriChadError> {
    delete_items::<table::Genre>(&fetcher, game_key, &genres).await
}

#[tauri::command]
pub async fn database_delete_languages(
    fetcher: tauri::State<'_, DatabaseFetcher>,
    game_key: GameKey<'_>,
    languages: Vec<String>,
) -> Result<(), TauriChadError> {
    delete_items::<table::Language>(&fetcher, game_key, &languages).await
}

#[tauri::command]
pub async fn database_delete_tags(
    fetcher: tauri::State<'_, DatabaseFetcher>,
    game_key: GameKey<'_>,
    tags: Vec<String>,
) -> Result<(), TauriChadError> {
    delete_items::<table::Genre>(&fetcher, game_key, &tags).await
}

#[tauri::command]
pub async fn database_upload_banner_url(
    fetcher: tauri::State<'_, DatabaseFetcher>,
    hash: String,
    banner_index: Option<usize>,
    banner_url: String,
) -> Result<(), TauriChadError> {
    fetcher
        .upload_banner_from_url(&hash, banner_index, &banner_url)
        .await
        .map_err(TauriChadError::from)
}

#[tauri::command]
pub async fn database_upload_banner_file(
    fetcher: tauri::State<'_, DatabaseFetcher>,
    hash: String,
    banner_index: Option<usize>,
    file: PathBuf,
) -> Result<(), TauriChadError> {
    fetcher
        .upload_banner_from_file(&hash, banner_index, &file)
        .await
        .map_err(TauriChadError::from)
}
