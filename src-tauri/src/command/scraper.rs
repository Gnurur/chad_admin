use std::collections::HashSet;

use chad_rs::{
    banner::BannerFetcher,
    database::{self, DatabaseFetcher, GetGamesOpts},
    scraper::leetx,
};
use futures::{future::join_all, prelude::*};
use serde::{Deserialize, Serialize};
use serde_json::json;
use tauri::Manager;
use uuid::Uuid;

use super::TauriChadError;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ActionMessage {
    id: String,
    action: UpdateAction,
}

impl ActionMessage {
    pub fn new(action: UpdateAction) -> Self {
        Self {
            id: Uuid::new_v4().to_string(),
            action,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(tag = "type")]
pub enum UpdateAction {
    AddGame {
        game: database::Game,
    },
    UpdateGame {
        from: database::Game,
        to: database::Game,
    },
    RemoveGame {
        game: database::Game,
    },
    AddBanner {
        game: database::Game,
        banners: Vec<String>,
        selected: Option<usize>,
    },
}

async fn apply_action(
    action: UpdateAction,
    database: &DatabaseFetcher,
) -> Result<(), TauriChadError> {
    match action {
        UpdateAction::AddGame { game } | UpdateAction::UpdateGame { to: game, from: _ } => database
            .add_update_game(
                &game.game,
                &game.languages.into_iter().collect::<Vec<_>>(),
                &game.genres.into_iter().collect::<Vec<_>>(),
                &game.tags.into_iter().collect::<Vec<_>>(),
            )
            .await
            .map_err(TauriChadError::from),
        UpdateAction::RemoveGame { game } => database
            .remove_game(&game.key())
            .await
            .map_err(TauriChadError::from),
        UpdateAction::AddBanner {
            game,
            banners,
            selected,
        } => {
            if let Some(banner) = selected.and_then(|s| banners.get(s)) {
                database
                    .upload_banner_from_url(&game.hash, game.banner_index, banner)
                    .await
                    .map_err(TauriChadError::from)
            } else {
                Err(TauriChadError::new("No banner selected".into()))
            }
        } //_ => Err(TauriChadError::new("Not implemented".into())),
    }
}

#[tauri::command]
pub async fn scraper_apply_actions(
    actions: Vec<ActionMessage>,
    database: tauri::State<'_, DatabaseFetcher>,
    app: tauri::AppHandle,
) -> Result<(), TauriChadError> {
    let futures = actions.into_iter().map(|message| async {
        let _ = match apply_action(message.action, &database).await {
            Ok(_) => app.emit_all("action_done", json! ({ "id": message.id })),
            Err(e) => app.emit_all("action_error", json! ({ "id": message.id, "error": e })),
        };
    });

    join_all(futures).await;

    Ok(())
}

async fn handle_scrape_stream(
    mut stream: impl Stream<Item = Result<leetx::Game, leetx::ScrapeError>> + std::marker::Unpin,
    remove_old: bool,
    database: &DatabaseFetcher,
    app: tauri::AppHandle,
) -> Result<(), TauriChadError> {
    let options = GetGamesOpts::default();
    let database_games = database.get_games(&options).await?;
    let mut scraped_hashes: HashSet<(String, String)> = HashSet::new();

    while let Some(item) = stream.next().await {
        match item {
            Ok(game) => {
                let scraped_game: database::Game = game.into();
                scraped_hashes.insert((scraped_game.hash.clone(), scraped_game.file.clone()));
                let db_game = database_games
                    .iter()
                    .find(|g| g.hash == scraped_game.hash && g.file == scraped_game.file)
                    .cloned();

                if let Some(db_game) = db_game {
                    // Already in database
                    if db_game != scraped_game {
                        // It's different, so we might want to update it
                        let _ = app.emit_all(
                            "scrape_new_action",
                            ActionMessage::new(UpdateAction::UpdateGame {
                                from: db_game,
                                to: scraped_game,
                            }),
                        );
                    } else {
                        //println!("Game already in database: {:?}", scraped_game.name);
                    }
                } else {
                    // Not in the database yet, so we might want to add it
                    let _ = app.emit_all(
                        "scrape_new_action",
                        ActionMessage::new(UpdateAction::AddGame { game: scraped_game }),
                    );
                }
            }
            Err(err) => {
                let _ = app.emit_all("scrape_game_err", err.to_string());
            }
        };
    }

    if remove_old {
        let db_hashes: HashSet<(String, String)> = database_games
            .iter()
            .map(|g| (g.hash.clone(), g.file.clone()))
            .collect();
        for (hash, file) in db_hashes.difference(&scraped_hashes) {
            if let Some(game) = database_games
                .iter()
                .find(|g| &g.hash == hash && &g.file == file)
            {
                let _ = app.emit_all(
                    "scrape_new_action",
                    ActionMessage::new(UpdateAction::RemoveGame { game: game.clone() }),
                );
            }
        }
    }

    Ok(())
}

#[tauri::command]
pub async fn scraper_get_new_games_pages(
    first_page: usize,
    num_pages: usize,
    scraper: tauri::State<'_, leetx::LeetxScraper>,
    database: tauri::State<'_, DatabaseFetcher>,
    app: tauri::AppHandle,
) -> Result<(), TauriChadError> {
    let games_stream = scraper.get_games_n_pages(first_page, num_pages);
    handle_scrape_stream(games_stream, false, &*database, app).await
}

#[tauri::command]
pub async fn scraper_get_all_new_games(
    scraper: tauri::State<'_, leetx::LeetxScraper>,
    database: tauri::State<'_, DatabaseFetcher>,
    app: tauri::AppHandle,
) -> Result<(), TauriChadError> {
    let games_stream = scraper.get_all_games().await?;
    handle_scrape_stream(games_stream, true, &*database, app).await
}

#[tauri::command]
pub async fn scraper_find_banners_for_game(
    game_name: &str,
    banner: tauri::State<'_, BannerFetcher>,
) -> Result<Vec<String>, TauriChadError> {
    banner
        .find_images(&game_name)
        .await
        .map_err(TauriChadError::from)
}
